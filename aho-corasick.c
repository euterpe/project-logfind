// Implementation of the Aho-Corasick substring search algorithm.

#include <stdlib.h>
#include "aho-corasick.h"
#include "ahoc-queue.h"
#include "dbg.h"

// Minimum of number of match lines to allocate in createMatchArray,
// plus 3 for storing current bufer size, tail index, and total number of matches.
#define BUF_SIZE 100

// Enum tracking indices for option values in options array; must match that specified in main.c
enum Options { OR, INSENSITIVE };

//--- CONSTRUCTORS ---//

// Construct the state machine detailed by Aho and Corasick in "Efficient String Matching: An
// Aid to Bibliographic Search".
//
// The "goto", "failure", and "output" functions are here integrated to create a single structure
// representing a completed state machine.
//
// The constructStateMachine function largely mirrors the "goto" function in its creation of
// the primary branches of the suffix tree. The "output" and "failure" functions are then
// constructed and stored as arrays. All of these elements are stored in the StateMachine structure.
//
// Actual searches are conducted by a separate function which reads the input text and runs it
// through the state machine to generate results.
//
// Preconditions: an array of null-terminated keywords and the size of that array
// Postconditions: returns a state machine that approximating that detailed by the Aho-Corasick
// algorithm that is suitable for use in text searches
StateMachine *constructStateMachine(char **keywords, int num_keywords)
{
  //check(keywords, "constructStateMachine: no keywords passed.");

  StateMachine *new_machine = malloc(sizeof(StateMachine));   // Allocate memory for new state machine
  check((new_machine != NULL), "constructStateMachine: failed to allocate new goto function.");

  new_machine->root = createStateNode(NULL, '\0', 0);    // Add a null-initialized root node
  check((new_machine->root != NULL), "constructStateMachine: failed to create new root node.");

  new_machine->root->suffixLink = new_machine->root;    // Set state 0 to loop on itself on failure state

  new_machine->keywords = malloc(num_keywords * sizeof(char *));    // Allocate an array to store keywords
  check((new_machine->keywords != NULL), "constructStateMachine: failed to create keyword array.");
  for (int i = 0; i < num_keywords; i++) {      // Copy each keyword into array
    int length = strlen(keywords[i]) + 1;
    new_machine->keywords[i] = malloc(length * sizeof(char *));
    strncpy(new_machine->keywords[i], keywords[i], length);
    //new_machine->keywords[i][length] = '\0';
  }

  // Find the sum of the lengths of all keywords
  int total_length = 0;
  for (int i = 0; i < num_keywords; i++) {
    int j = 0;
    while (keywords[i][j] != '\0') {
      total_length++;
      j++;
    }
  }

  // The maximum number of elements in the output function is the sum of the lengths of all keywords.
  // Allocate one pointer per character of total length and set to NULL.
  new_machine->output = calloc((total_length + 1), (sizeof(struct OutputList*)));   // Add 1 to account for state 0
  check((new_machine->output != NULL), "constructStateMachine: failed to allocate output array");
  for (int i = 0; i < total_length; i++) {
    new_machine->output[i] = NULL;
  }
  
  // Begin "goto" construct a.k.a. "Algorithm 2" //

  int curr_state_id = 0;    // Create int to track series of state numbers
  for (int i = 0; i < num_keywords; i++) {    // Add each keyword to the goto function graph
    int rc = enter(keywords[i], i, &curr_state_id, new_machine);
    check((rc == 0), "constructStateMachine: enter() failed on keyword %d.", i);
  }

  // Set keyword and state totals
  new_machine->totalKeywords = num_keywords;
  new_machine->totalStates = curr_state_id;

  // The loop to set g(0, a) = 0 for every g(0, a) = fail is excluded as superfluous.
  // Rather than create each of these as unique states set to 0, any case of g(0, a)
  // will simply reset the current node to the root node, i.e. to 0.

  int rc = constructFailureF(new_machine);    // Create failure array/condense failure transition chains
  check((rc == 0), "constructStateMachine: failed to create failure function");

  return new_machine;

error:
  if (new_machine) {    // Clean up on error
    cleanStateMachine(new_machine);
  }
  return NULL;      // Return NULL on error
}

// Preconditions: 1) a state machine with at least one node (root)
//                2) a character denoting the edge connecting the parent node to the new one
//                3) a unique number identifying the new state node
// Postconditions: returns a new state node initialized with the passed data and defaults for 
// all other values
StateNode *createStateNode(StateNode *parent, char symbol, int state)
{
  StateNode *new_node = malloc(sizeof(StateNode));   // Allocate new state node
  check((new_node != NULL), "Failed to allocate new StateNode for state id %d.", state);

  new_node->parent = parent;              // Link node back to its parent

  for (int i = 0; i < MAX_CHARS; i++) {   // For each of the MAX_CHARS possible characters
    new_node->children[i] = NULL;         // Initialize a child link to NULL
  }

  new_node->suffixLink = NULL;            // Initialize pointer to suffix link to NULL
  new_node->edgeLabel = symbol;              // Set edge label to passed symbol
  new_node->stateId = state;              // Set state number from passed state

  // wordId maps the word represented by a leaf node to the corresponding keyword array index
  new_node->wordId = -1;    // Negative values mark state nodes that do NOT terminate keywords

  return new_node;

error:
  if (new_node) {       // Release newly allocated memory on failure
    free(new_node);
  }
  return NULL;
}

// Preconditions: keyword string initialized and mapped to an index in an array of keywords
// Postconditions: returns a pointer to a newly allocated linked list for outputNodes,
// its head initialized with a new outputNode with the passed keyword data
struct OutputList *createOutputList(char *keyword, int keyword_index)
{
  struct OutputList *new_output_list = malloc(sizeof(struct OutputList));   // Allocate memory for new list
  check((new_output_list != NULL), "createOutputList: failed to allocate new OutputList for keyword %s.",
      keyword);

  new_output_list->head = createOutputNode(keyword, keyword_index);     // Create new head node with passed keyword data
  check((new_output_list->head != NULL), "createOutputList: failed to allocate new OutputNode in OutputList for keyword %s.",
      keyword);

  new_output_list->tail = new_output_list->head;      // Point tail to the same node as head: only one node on creation

  return new_output_list;

error:
  if (new_output_list->head) {        // On error..
    free(new_output_list->head);      // Free any memory allocated in this function
  }
  if (new_output_list) {
    free(new_output_list);            // Return NULL to indicate error condition
  }
  return NULL;
}

// Preconditions: keyword string initialized and mapped to an index in an array of keywords
// Postconditions: a new node with output information created for addition to the linked list 
// in position keyword_index of output function array
struct OutputNode *createOutputNode(char *keyword, int keyword_index)
{
  struct OutputNode *new_output_node = malloc(sizeof(struct OutputNode));
  check((new_output_node != NULL), "createOutputNode: failed to allocate new OutputNode for keyword %s.",
      keyword);
  new_output_node->next = NULL;     // Set next pointer to NULL: new node will become the tail of the list

  size_t length = strlen(keyword) + 1;                        // Find length of the keyword and add space for '\0'
  new_output_node->keyword = malloc(length * sizeof(char));   // Allocate memory to store the keyword in the node

  strncpy(new_output_node->keyword, keyword, length);    // Copy keyword into node
  //new_output_node->keyword[length] = '\0';

  new_output_node->wordId = keyword_index;               // Update wordId to match the keyword_index and preserve mapping

  return new_output_node;

error:
  if (new_output_node->keyword) {         // On error..
    free(new_output_node->keyword);       // Free any memory allocated in this function
  }
  if (new_output_node) {
    free(new_output_node);
  }
  return NULL;                            // Return NULL to indicate error condition
}
// Preconditions: total number of search keywords
// Postconditions: returns array of num_keywords Matches each initialized to 0
//
// The match array stores the match information for each keyword. The array is
// indexed identically to the keyword array of the state machine itself.
Match **createMatchArray(int num_keywords) {
  Match **new_match_array = calloc(num_keywords, sizeof(struct MatchContainer));
  check((new_match_array != NULL), "createMatchArray: failed to allocate array");

  for (int i = 0; i < num_keywords; i++) {
    new_match_array[i] = createMatch();
    check((new_match_array[i] != NULL), "createMatchArray: failed to allocate MatchContainer %d", i);
  }

  return new_match_array;

error:
  if (new_match_array) {
    cleanMatchArray(new_match_array, num_keywords);
  }
  return NULL;
}

// Preconditions: none
// Postconditions: returns a single Match structure with all members initialized to 0
Match *createMatch(void) {
  Match *new_match = calloc(1, sizeof(Match));
  check((new_match != NULL), "createMatch: MatchContainer allocation failed");

  new_match->lineNumbers = calloc(BUF_SIZE, sizeof(int));
  check((new_match != NULL), "createMatch: dynamic array allocation failed");

  new_match->size = BUF_SIZE;
  new_match->tail = 0;
  new_match->totalMatches = 0;

  return new_match;

error:
  if (new_match) {
    free(new_match);
  }
  return NULL;
}

//--- DESTRUCTORS ---//

void cleanStateMachine(StateMachine *tree)
{
  if (tree) {
    if (tree->root) {
      cleanStateNode(tree->root);
      free(tree->root);
    }

    if (tree->output) {
      for (int i = 0; i <= tree->totalStates; i++) {
        if (tree->output[i]) {
          cleanOutputList(tree->output[i]);
          free(tree->output[i]);
        }
      }
      free(tree->output);
    }

    if (tree->keywords) {
      for (int i = 0; i < tree->totalKeywords; i++) {
        if (tree->keywords[i]) {
          free(tree->keywords[i]);
        }
      }
      free(tree->keywords);
    }
    free(tree);
  }
}

void cleanStateNode(StateNode *node)
{
  if (node) {
    if (node->children) {
      for (int i = 0; i < MAX_CHARS; i++) {
        if (node->children[i]) {
          cleanStateNode(node->children[i]);
          free(node->children[i]);
        }
      }
    }
  }
}

void cleanOutputList(struct OutputList *output_list)
{
  if (output_list) {
    cleanOutputNode(output_list->head);
    free(output_list->head);
  }
}

void cleanOutputNode(struct OutputNode *output_node)
{
  if (output_node->next) {
    cleanOutputNode(output_node->next);
    free(output_node->next);
  } else {
    if (output_node->keyword) {
      free(output_node->keyword);
    }
  }
}

void cleanMatchArray(Match **matches, int num_keywords)
{
  if (matches) {
    for (int i = 0; i < num_keywords; i++) {
      if (matches[i]) {
        cleanMatch(matches[i]);
        free(matches[i]);
      }
    }
    free(matches);
  }
}

void cleanMatch(Match *match)
{
  if (match) {
    if (match->lineNumbers) {
      free(match->lineNumbers);
    }
  }
}

//-- ALGORITHM FUNCTIONS --//

// This function implements the "enter" function used in "Algorithm 2" of the Aho-Corasick
// search algorithm.
//
// Preconditions: 1) a null-terminated keyword string
//                2) the index to which the keyword is mapped in the keyword array
//                3) a pointer to the state id counter
//                4) an initialized state machine
// Postconditions: a series of nodes representing a single keyword is added to the state
// machine and the output array of the machine is updated with the new keyword
int enter(char *keyword, int keyword_index, int *curr_state_id, StateMachine *new_machine)
{
  StateNode *state = new_machine->root;   // Always begin building new keyword links/suffixes from root
                                          // This corresponds to "state <-- 0" in original algorithm.
  check((state != NULL), "enter: state set to invalid pointer value (NULL).");

  int j = 0;    // Index of current character in keyword; corresponds to "j <-- 1"

  // Loop through all non-fail states from root node (start = 0).
  // This effectively skips all existing nodes that form a prefix of
  // the keyword, allowing all new nodes to be inserted on a new branch
  // where the first divergent character of the keyword is found.
  while(! isFail(state, keyword[j])) {
    state = state->children[(unsigned char)keyword[j]];
    j++;
  }

  int length = strlen(keyword);   // Find the length of the keyword to be added
  check((length > 0), "enter: invalid keyword length.");

  // Add remaining keyword characters from existing node representing a prefix of the keyword (if any)
  for (int p = j; p < length; p++) {
    (*curr_state_id) += 1;    // Increment current state id to match next state to be inserted

    // Insert new state node into graph as child of existing state node, mapped by numeric ASCII code
    state->children[(unsigned char)keyword[p]] = createStateNode(state, keyword[p], *curr_state_id);
    check((state->children[(unsigned char)keyword[p]] != NULL), "enter: failed to create new child node of state %d.", *curr_state_id);

    state = state->children[(unsigned char)keyword[p]];   // Set the state to the newly added node for next iteration and insertion

    // The state active when the end of the keyword is reached is a leaf that
    // corresponds to a full keyword match. Set wordId to allow direct access
    // to the keyword from that state and add that keyword to the output function array.
    if (p+1 == length) {
      state->wordId = keyword_index;

      // Add keyword to output function array by creating a new outputList initialized with keyword data
      check((new_machine->output != NULL), "enter: passed uninitialized output function");
      new_machine->output[state->stateId] = createOutputList(keyword, keyword_index);
      check((new_machine->output[state->stateId] != NULL), "enter: failed to create new OutputList for keyword.");
    }
  }

  return 0;

error:
  return 1;     // Return 1 on error
}

// Helper function to check whether a given state-character pair results
// in a fail outcome, i.e. the character does not exist on a child edge
// from state.
//
// Syntax modeled after goto function of "Algorithm 2": g(state, a).
//
// Preconditions: an initialized stateNode and character to check against
// Postconditions: returns 1 if the pair results in a "fail" and 0 if not.
int isFail(StateNode *state, char a)
{
  check(state != NULL, "state doesn't exist");
  if (state->children[(unsigned char)a] == NULL) {
    return 1;
  } else {
    return 0;
  }
error:
  return -1;    // Return -1 on error
}

// Precondition: state machine has been allocated and initialized with at least one non-root state
// Postcondition: an outputNode is appended to the list in the output array corresponding to the
// keyword represented by the passed state node
int addOutputNode(StateMachine *new_machine, StateNode *state)
{
  int stateId = state->stateId;   // Store stateId in temporary shorthand

  // Create the new node
  struct OutputNode *new_output_node = createOutputNode(new_machine->keywords[state->wordId], state->wordId);
  check((new_output_node != NULL), "addOutputNode: new node creation failed");

  new_machine->output[stateId]->tail->next = new_output_node;    // Add the node to the end of the list
  new_machine->output[stateId]->tail = new_output_node;          // Update the tail to reflect the addition
  
  return 0;

error:
  return 1;     // Return 1 on error
}

// This function implements "Algorithm 3" of the Aho-Corasick search algorithm. It condenses the
// chain of failure states (i.e. suffix links) between state nodes into direct links, decreasing
// traversal time for failure state transitions.
//
// As in the original function, this implementation uses a queue (type specific for StateNodes).
//
// Preconditions: a state machine has been created and initialized
// Postconditions: adds an array of state nodes representing failure transitions to the StateMachine
// structure for use during search calls.
int constructFailureF(StateMachine *new_machine)
{
  Queue *queue = createQueue();

  // For every keyword character that exists from root (i.e. each of its
  // children), add it to the queue of nodes yet to be checked for failure
  // transitions, and set its failure state to the root node.
  for (int i = 0; i < MAX_CHARS; i++) {
    if (new_machine->root->children[i] != NULL) {
      queuePush(queue, new_machine->root->children[i]);
      new_machine->root->children[i]->suffixLink = new_machine->root;
    }
  }

  StateNode *r = NULL;   // Next state in the queue, per Algorithm 3
  StateNode *s = NULL;   // Transition state from r, per Algorithm 3

  while (queue->head != NULL) {   // While there are nodes in the queue
    r = queuePop(queue);          // Select and remove a node from the queue

    for (int i = 0; i < MAX_CHARS; i++) {   // For each child of the node selected
      s = r->children[i];
      if (s != NULL) {                      // Check whether it exists
        queuePush(queue, s);                // If so, add to queue
        StateNode *state = r->suffixLink;   // Create handle for suffix link(s) of selected node

        // Follow chain of suffix links from selected node to root or node representing a valid suffix,
        // i.e. a node with a child matching the ASCII character with numeric code == i
        while (state->children[i] == NULL && state != new_machine->root) { state = state->suffixLink; }
        
        // If while loop terminated at NULL, no suffix links were found: set suffix link of s (current child
        // of r) to root. If not, set that suffix link to the state node active when the loop terminated.
        if (state->children[i] == NULL) {
          s->suffixLink = new_machine->root;
        } else {
          s->suffixLink = state->children[i];
        }

        // Not sure what is happening here...
        if (s->suffixLink->wordId < 0 && s->suffixLink->wordId > new_machine->totalKeywords) {
          int rc = addOutputNode(new_machine, s->suffixLink);
          check((rc == 0), "constructFailureF: failed to add failure function to output");
        }
      }
    }
  }
  deleteQueue(&queue);    // Clean up
  
  return 0;

error:
  if (queue) {            // Clean up queue on error
    deleteQueue(&queue);
  }
  return 1;               // Return 1 on error
}

//--- USER FUNCTIONS ---//

// Search a single text file by applying "Algorithm 1" of Aho-Corasick.
//
// Preconditions: a stream has been connected to a valid text file, an array
// of keyword strings has been initialized, and the size of that array is set
// Postconditions: the results of the search are formatted and printed to stdout
// file-by-file and keyword-by-keyword
int fileSearch(FILE *stream, char **keywords, int num_keywords)
{
  // Construct state machine using Aho-Corasick algorithm
  StateMachine *stateMachine = constructStateMachine(keywords, num_keywords);
  check((stateMachine != NULL), "fileSearch: failed to create state machine");
  StateNode *state = stateMachine->root;   // Set start state = 0 per "Algorithm 1"

  // Array of structures containing dynamic arrays of ints. Each struct stores the total
  // number of matches for a keyword and a dynamic array of line numbers where those matches are found.
  Match **matches = createMatchArray(num_keywords);

  int lineno = 1;     // Line number counter
  char c = '\0';      // Initialize to null
  while ((c = fgetc(stream)) && c != EOF) {     // While there are characters left in the input file
    if (c == '\n') { lineno++; }

    // Follow trail of suffix links back to first non-fail state
    while (state->children[(unsigned char)c] == NULL    // Check for path from current state to input character
        && state->suffixLink != stateMachine->root) {   // Break loop if state becomes root (g(state, c) is always the same at root)
      state = state->suffixLink;
    }

    state = state->children[(unsigned char)c];    // Set state to child of the first non-fail state and continue from there
    if (state == NULL) {                          // state becomes NULL if first non-fail state is root and c is not a child
      state = stateMachine->root;                 // Reset state to root in such a case
    } else {
      if (stateMachine->output[state->stateId] != NULL) {                 // Check if output function is empty for current state
        addOutputToMatches(stateMachine->output[state->stateId], matches, lineno);    // Print keywords in output function for current state
      }
    }
  }

  printMatchesO(stateMachine, matches);   // Print output matches using OR logic

  cleanMatchArray(matches, num_keywords);   // Clean up
  cleanStateMachine(stateMachine);

  return 0;

error:
  if (stateMachine) {     // Clean up on error
    cleanStateMachine(stateMachine);
  }

  return 1;   // Return 1 on error
}

// Search multiple text files for given keywords using state machine constructed
// according to Aho-Corasick algorithm. 
//
// Runs each input text file from pathnames array through the machine, prints the results,
// then cleans up.
//
// Preconditions: 1) an array of null-terminated pathname strings
//                2) the number of pathnames in the array
//                3) an array of null-terminated keyword strings
//                4) the number of keywords in the array
//                5) an array of ints containing options as defined by global enum
// Postconditions: all files are searched for all keywords and results are formatted and printed
// according to the specified options
int multiFileSearch(char **pathnames, int num_paths, char **keywords, int num_keywords, int *options) {

  Match **matches = NULL;   // Initialize match array to NULL
  StateMachine *stateMachine = constructStateMachine(keywords, num_keywords);   // Create state machine
  check((stateMachine != NULL), "fileSearch: failed to create state machine");
  StateNode *state = stateMachine->root;   // Set start state = 0 per "Algorithm 1"


  for (int i = 0; i < num_paths; i++) {
    // Array of structures containing dynamic arrays of ints. Each struct stores the total
    // number of matches for a keyword and a dynamic array of line numbers where those matches are found.
    matches = createMatchArray(num_keywords);
    check((matches != NULL), "multiFileSearch: failed to create match array for \"%s\"", pathnames[i]);

    FILE *stream = fopen(pathnames[i], "r");
    //check((stream != NULL), "multiFileSearch: failed to open file stream for \"%s\"", pathnames[i]);
    
    if (stream != NULL) {     // Check for valid stream
      printf(">>RESULTS for \"%s\":\n", pathnames[i]);

      int lineno = 1;     // Line number counter
      char c = '\0';
      while ((c = fgetc(stream)) && c != EOF) {     // While there are characters left in the input file
        if (c == '\n') { lineno++; }

        // Follow trail of suffix links back to first non-fail state
        while (state->children[(unsigned char)c] == NULL    // Check for path from current state to input character
            && state->suffixLink != stateMachine->root) {   // Break loop if state becomes root (g(state, c) is always the same at root)
          state = state->suffixLink;
        }

        state = state->children[(unsigned char)c];    // Set state to child of the first non-fail state and continue from there
        if (state == NULL) {                          // state becomes NULL if first non-fail state is root and c is not a child
          state = stateMachine->root;                 // Reset state to root in such a case
        } else {
          if (stateMachine->output[state->stateId] != NULL) {                 // Check if output function is empty for current state
            addOutputToMatches(stateMachine->output[state->stateId], matches, lineno);    // Add output to match array for later printing
          }
        }
      }
      fclose(stream);

      if (options[OR] == 1) {    // Change printout depending on option specifying AND/OR matching
        printMatchesO(stateMachine, matches);
      } else {
        printMatchesA(stateMachine, matches);
      }
    }
    cleanMatchArray(matches, num_keywords);   // Clean match array for next file search
  }

  cleanStateMachine(stateMachine);    // Clean up

  return 0;

error:
  if (stateMachine) {   // Clean up on error
    cleanStateMachine(stateMachine);
  }
  if (matches) {
    cleanMatchArray(matches, num_keywords);
  }

  return 1;   // Return 1 on error
}


//-- SUPPORT FUNCTIONS --//

// Add current line number to match record for each keyword of the passed output list
// and increment the total number of matches for that keyword. 
// This function primarily provides context and acts as a kind of wrapper for pushLineNo.
//
// Preconditions: an initialized OutputList and Match array, and the line number to be added
// Postconditions: the line number is added to the match record for each search keyword and
// the match total for each record is updated
int addOutputToMatches(struct OutputList *list, Match **matches, int lineno)
{
  check(list != NULL, "output list does not exist.");
  check(matches != NULL, "match array does not exist.");

  struct OutputNode *curr_node = list->head;    // Create tempory node for iterating through the list
  int rc = 0;

  do {    // Add the line number to the dynamic array in each node of the list
    rc = pushLineNo(matches, curr_node->wordId, lineno);
    check((rc == 0), "addOutputToMatches: failed to add line number to dynamic array");
    curr_node = curr_node->next;
  } while (curr_node != NULL);

  return 0;

error:
  return 1;   // Return 1 on error
}

// Push a new line number to the tail of a dynamic array in the matches array.
// If the array is full, resize before adding the new line number.
//
// Preconditions: an initialized Match array, an index corresponding to the target
// keyword, and the line number to add
// Postconditions: a new line number is added to the end of the dynamic array
int pushLineNo(Match **matches, int i, int lineno) {
  check(matches != NULL, "match array doesn't exist");
  check(matches[i] != NULL, "no keyword at index %d in match array", i);

  // Create local shorthands for readability
  int tail = matches[i]->tail;
  int size = matches[i]->size;

  if (tail >= size-1) {     // If tail is at the final allocated position
    // Double dynamic array size
    matches[i]->lineNumbers = (int *) realloc(matches[i]->lineNumbers, ((size * 2) * sizeof(int)));
    matches[i]->size *= 2;
  }

  // 'Base case': manually set first position of empty dynamic array
  if (matches[i]->lineNumbers[tail] == 0 && tail == 0) {
    matches[i]->lineNumbers[tail] = lineno;
  }

  if (matches[i]->lineNumbers[tail] != lineno) {      // If new line number doesn't exist in the array
      matches[i]->lineNumbers[tail+1] = lineno;       // Add the new line number to the end of the array
      matches[i]->tail += 1;                          // Set the tail index to match the new end of the array
  }

  matches[i]->totalMatches += 1;    // Increment totalmatches to reflect addition

  return 0;
error:
  return 1;   // Return 1 on error
}

// Preconditions: a complete state machine and match array
// Postconditions: formats and prints all match data for each keyword
// matched using OR logic
void printMatchesO(StateMachine *stateMachine, Match **matches)
{
  int matched_words = 0;    // Count number of matched keywords
  for (int i = 0; i < stateMachine->totalKeywords; i++) {
    if (matches[i]->totalMatches > 0) {
      matched_words++;
    }
  }

  if (matched_words > 0) {    // If any word matched
    for (int i = 0; i < stateMachine->totalKeywords; i++) {   // For a search keyword
      printf("\nMATCHES for \"%s\": %d\n", stateMachine->keywords[i], matches[i]->totalMatches);
      if (matches[i]->totalMatches > 0) {
        printf("ON LINES:\n");
        for (int j = 0; j <= matches[i]->tail; j++) {    // Print all line numbers where matches are found
          if (j != 0 && j % 5 == 0) {                    // Print 5 matches per line
            printf("\n");
          }
          printf(" %7d", matches[i]->lineNumbers[j]);    // Set 7 character width to accommodate large numbers of matches
        }
        printf("\n");
      }
    }
  } else {
    printf("No matches found.\n");    // Notify if no matches were found
  }
  printf("\n----------\n");
}

// Preconditions: a complete state machine and match array
// Postconditions: formats and prints all match data for each keyword
// matched using AND logic
void printMatchesA(StateMachine *stateMachine, Match **matches)
{
  int matched_words = 0;    // Count number of matched keywords
  for (int i = 0; i < stateMachine->totalKeywords; i++) {
    if (matches[i]->totalMatches > 0) {
      matched_words++;
    }
  }

  if (matched_words == stateMachine->totalKeywords) {         // If every word has at least one match, print match data
    for (int i = 0; i < stateMachine->totalKeywords; i++) {   // For a search keyword
      printf("MATCHES for \"%s\": %d\n", stateMachine->keywords[i], matches[i]->totalMatches);
      if (matches[i]->totalMatches > 0) {
        printf("ON LINES:\n");
        for (int j = 0; j <= matches[i]->tail; j++) {    // Print all line numbers where matches are found
          if (j != 0 && j % 5 == 0) {                    // Print 5 matches per line
            printf("\n");
          }
          printf(" %7d", matches[i]->lineNumbers[j]);    // Set 7 character width to accommodate large numbers of matches
        }
        printf("\n");
      }
    }
  } else {
    printf("One or more keywords missing from file.\n");    // Notify if no matches were found
  }
  printf("\n----------\n");
}

/* OLD TEST FUNCTIONS
void printOutputList(struct OutputList *list, int lineno)
{
  struct OutputNode *curr_node = list->head;
  printf("\tKeywords: %s", curr_node->keyword);
  while (curr_node->next != NULL) {
    curr_node = curr_node->next;
    printf(" %s", curr_node->keyword);
  }
  printf("\n\tLine: %d\n", lineno);
}

// Prints tree in depth-first order
void DFS_print(StateMachine *tree, StateNode *node)
{
  printf("State: %d, Symbol: %c, Failure: %d\n", node->stateId, node->edgeLabel, node->suffixLink->stateId);
  if (node->wordId >= 0) {
    printf("Keyword match: %s\n", tree->keywords[node->wordId]);
  }
  for (int i = 0; i < MAX_CHARS; i++) {
    if (node->children[i] != NULL) {
      DFS_print(tree, node->children[i]);
    }
  }
}

// Prints the output function
void output_print(StateMachine *tree)
{
  struct OutputNode *curr_node = NULL;
  for (int i = 0; i <= tree->totalStates; i++) {
    if (tree->output[i] != NULL) {
      curr_node = tree->output[i]->head;
      printf("--> STATE %d:\n%s\n", i, curr_node->keyword);
      while (curr_node->next != NULL) {
        curr_node = curr_node->next;
        printf("%s\n", curr_node->keyword);
      }
    }
  }
}
*/
