CC := gcc
RM := rm -f
CFLAGS := -g -Wall -Wextra -DNDEBUG

SRCS := main.c aho-corasick.c ahoc-queue.c
OBJS := $(subst .c,.o,$(SRCS))

# Link program
logfind: $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS)

main.o: main.c aho-corasick.h

aho-corasick.o: aho-corasick.c aho-corasick.h ahoc-queue.h

ahoc-queue.o: ahoc-queue.c aho-corasick.h

.PHONY: clean
clean:
	$(RM) $(OBJS)

.PHONY: distclean
distclean: clean
	$(RM) logfind
