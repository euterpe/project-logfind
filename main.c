#include <stdlib.h>
#include <stdio.h>
#include <glob.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "aho-corasick.h"
#include "dbg.h"

#define CFG_PATH ".logfindrc"
#define MAX_BUF 100     // Max input line length for readline
#define PATH_BUF 100    // Max pathname length for readPathnames
#define MAX_OPS 2       // Total number of options tracked

// Position assignments for options array; number of assignments is equal to MAX_OPS
enum Options { OR, INSENSITIVE };

// Wrapper for standard fgets() that removes newline character.
// Preconditions: a non-NULL string pointer and file stream pointer
// Postconditions: a line from the file connected to stream is read into
// the string pointer and an exit status is returned
int readline(char *lineptr, FILE *stream)
{
  check(lineptr != NULL, "line pointer does not exist.");
  char *result = fgets(lineptr, MAX_BUF, stream);

  if (result != NULL) {
    lineptr[strcspn(lineptr, "\n")] = '\0';
    return 1;   // Return 1 if input has been read
  } else {
    return 0;   // Return 0 if blank line or EOF
  }
error:
  return -1;    // Return -1 on error
}

// Confirm that [string] is a valid pathname. Return non-zero if valid.
int is_valid_path(const char *string)
{
  // Create stat struct to hold file info
  struct stat *file_info = calloc(1, sizeof(struct stat));

  // Check string for invalid characters
  char prev = '\0';
  for (int i = 0; string[i] != '\0'; i++) {
    // Valid pathnames must consist only of A-Z a-z 0-9 . _ - and / (for directories)
    check((isalnum(string[i]) || string[i] == '/' || string[i] == '.' || string[i] == '_' || string[i] == '-'),
        "Invalid pathname: contains %c", string[i]);
    check(!(string[i] == '/' && prev == '/'), "Invalid pathname: inclusion of \"//\".");
    prev = string[i];
  }

  // Check if file at pathname is a regular file
  int rc = stat(string, file_info);
  check((rc == 0), "is_valid_path: failed to check filetype of %s", string);
  check((S_ISREG(file_info->st_mode) != 0), "is_valid_path: %s is not a regular file", string);

  // Release stat structure
  free(file_info);

  return 1;
error:
  if (file_info) {
    free(file_info);
  }
  return 0;
}

// Free memory from pathnames array
void cleanPathnames(char **pathnames, int num_paths)
{
  if (pathnames) {
    for (int i = 0; i < num_paths; i++) {
      if (pathnames[i]) {
        free(pathnames[i]);
      }
    }
    free(pathnames);
  }
}

// Free memory from keyword array
void cleanKeywords(char **keywords, int num_keywords)
{
  if (keywords) {
    for (int i = 0; i < num_keywords; i++) {
      if (keywords[i]) {
        free(keywords[i]);
      }
    }
    free(keywords);
  }
}


// Preconditions: CFG_PATH macro defined to valid path
// Postconditions: Array of strings representing pathnames is returned; num_paths updated
// to reflect the total number of paths read by glob()
char **readPathnames(int *num_paths)
{
  FILE *log_stream = fopen(CFG_PATH, "r");      // Open file stream
  char *line = calloc(MAX_BUF, sizeof(char));   // Allocate a buffer to store lines read
  char **pathnames = NULL;                      // Initialize pathname array to NULL

  glob_t *globbuf = calloc(1, sizeof(glob_t));       // Initialize glob buffer to store path names
  globbuf->gl_pathc = 0;                             // Initialize path count to 0

  int globrc = 0;   // Callback result for glob()
  int rc = 1;       // Callback result for readline(); stores the length of the last line read
  while (rc > 0) {
    rc = readline(line, log_stream);    // Read a new line from the stream

    if (rc > 0) {
      if (globbuf->gl_pathc > 0) {    // If not the first line read, add append flag
        globrc = glob(line, GLOB_NOSORT | GLOB_APPEND, NULL, globbuf);
        check((globrc == 0 || globrc == GLOB_NOMATCH), "readPathnames: glob function failed");
      } else {
        globrc = glob(line, GLOB_NOSORT, NULL, globbuf);
        check((globrc == 0 || globrc == GLOB_NOMATCH), "readPathnames: glob function failed");
      }
    }
  }
  fclose(log_stream);
  free(line);

  // Copy matched paths from glob buffer into pathname array
  pathnames = calloc(globbuf->gl_pathc, sizeof(char *));
  for (size_t i = 0; i < globbuf->gl_pathc; i++) {
    pathnames[i] = strdup(globbuf->gl_pathv[i]);
  }

  *num_paths  = globbuf->gl_pathc;   // Set number of paths to total count in glob struct

  globfree(globbuf);    // Release glob struct memory
  free(globbuf);

  return pathnames;

error:
  if (log_stream) { fclose(log_stream); }   // Clean up on error
  if (line) { free(line); }
  if (pathnames) {
    cleanPathnames(pathnames, globbuf->gl_pathc);
  }
  if (globbuf) {
    globfree(globbuf);
    free(globbuf);
  }

  return NULL;    // Return NULL on error
}

// Store passed option settings in array and return total number of
// options passed.
//
// Preconditions: a stack-allocated array to store options and a pointer
// to command line arguments read into argv
// Postconditions: flags for selected options set in options array
int getOptions(int options[], char *argv[])
{
  check(argv != NULL, "argv is empty");
  check(argv[1] != NULL, "received no command line arguments");
  int op_count = 0;     // Count number of options passed
  for (int i = 1; argv[i][0] == '-'; i++) {
    char c = argv[i][1];

    switch (c) {
      case 'o':
        options[OR] = 1;
        op_count++;
        break;
      case 'i':
        options[INSENSITIVE] = 1;
        op_count++;
        break;
      default:
        printf("logfind: passed invalid option\n");
        return -1;
    }
  }
  return op_count;
error:
  return -1;
}

// Converts keywords from argv to new array beginning at position 0.
// Preconditions: argc and argv initialized from command line, number of options
// set to non-negative value by getOptions
// Postconditions: return pointer to keyword array
char **getKeywords(int argc, char *argv[], int num_options)
{
  check(argv != NULL, "argv doesn't exist");
  check(argc > 1, "no keywords passed");
  int offset = 1 + num_options;       // Number of positions from 0 where keyword list begins
  int num_keywords = argc - offset;   // Total number of keywords excluding options and command name

  char **keywords = calloc(num_keywords, sizeof(char *));   // Allocate keyword array
  check((keywords != NULL), "getKeywords: failed to create keywod array");
  
  for (int i = 0; i < num_keywords; i++) {
    keywords[i] = strdup(argv[i+offset]);   // Add keyword to array via duplication (auto-malloced)
  }

  return keywords;

error:
  cleanKeywords(keywords, num_keywords);
  return NULL;
}

int main(int argc, char *argv[])
{
  char **pathnames = NULL;
  char **keywords = NULL;
  int *options = calloc(MAX_OPS, sizeof(int));

  // Create array for pathname strings
  int num_paths = 0;
  pathnames = readPathnames(&num_paths);
  check((num_paths > 0), "logfind: no valid pathnames found");

  // Retrieve options (if any)
  int num_ops = getOptions(options, argv);
  check(num_ops >= 0, "no valid options received");

  // Create array for keyword strings
  keywords = getKeywords(argc, argv, num_ops);
  check((keywords != NULL), "logfind: failed to retrieve keywords");
  int num_keywords = (argc - 1) - num_ops;

  // Conduct search across files stored in CFG_PATH using keywords from argv
  printf("logfind: searching logfiles in %s...\n", CFG_PATH);
  printf("\n----------\n");
  int rc = multiFileSearch(pathnames, num_paths, keywords, num_keywords, options);
  check((rc == 0), "logfind: multiple file search failed");

  // Clean up allocated memory
  cleanPathnames(pathnames, num_paths);
  cleanKeywords(keywords, num_keywords);
  free(options);

  return 0;

error:
  if (pathnames) {
    cleanPathnames(pathnames, num_paths);
  }
  if (keywords) {
    cleanKeywords(keywords, num_keywords);
  }
  if (options) {
    free(options);
  }
  return 1;
}
